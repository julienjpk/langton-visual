# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#
# Tables coming from https://github.com/GollyGang/ruletablerepository.


class CellularAutomata:
    def __init__(self, rules: str, initial: str):
        """
        Initialises an in-memory grid representing the state of the automata.
        The rules file will also be parsed into a dictionary.
        :param rules: The rules file.
        :param initial: The file containing the initial grid.
        """
        self.rules = {}
        self.automata = []

        # Parsing the rules.
        # The rules are stored in a dictionary, with their first character
        # (current state) as the key. This speeds up rule matching later on.
        with open(rules) as rules_file:
            for rule in rules_file:
                rule = rule.strip()
                if len(rule) != 6:
                    raise ValueError("Invalid rule: %s." % rule)
                else:
                    rule_start = int(rule[0])
                    if rule_start not in self.rules:
                        self.rules[rule_start] = []
                    self.rules[rule_start].append(rule[1:])

        # Parsing the initial grid.
        with open(initial) as initial_file:
            for row in initial_file:
                self.automata.append([int(c) for c in list(row.strip())])

        for row in self.automata:
            if len(row) != len(self.automata[0]):
                raise AttributeError("Invalid initial grid "
                                     "(should be a rectangle).")

    def grid(self):
        """
        Returns the current state of the automata.
        The grid should NOT be modified externally. This is step()'s job.
        :return: list The automata (2D).
        """
        return self.automata

    def get_neighbours(self, y: int, x: int):
        """
        Returns a list of the neighbours for the cell at (x,y). If the cell is
        at a limit, the neighbour will be set to None.
        :param y: int The y-coordinate (row) of the cell.
        :param x: int The x-coordinate (column) of the cell.
        :return: list A list of 4 neighbours, possibly some Nones.
        """
        coords = ((x, y - 1), (x + 1, y), (x, y + 1), (x - 1, y))
        n = []
        for xi, yi in coords:
            valid = 0 <= yi < len(self.automata) and \
                0 <= xi < len(self.automata[yi])
            n.append(self.automata[yi][xi] if valid else None)
        return n

    def get_future_cell(self, y: int, x: int):
        """
        Should NOT be called externally. Returns the future state of a given
        cell, as well as a list of directions for grid expansion. This is where
        all the rule-matching occurs.
        :param y: int The y-coordinate (row) of the cell.
        :param x: int The x-coordinate (column) of the cell.
        :return: int,list The future state of the cell, and a list of directions
                 in which the automata should be expanded/padded next time.
        """
        if self.automata[y][x] not in self.rules:
            return self.automata[y][x], None

        neighbours = self.get_neighbours(y, x)
        expand_map = [i for i in range(len(neighbours))
                      if neighbours[i] is None]

        if len(expand_map) == 0:
            expand_map = None
        else:
            for direction in expand_map:
                neighbours[direction] = 0

        rule_pattern = ''.join([str(n) for n in neighbours])
        alt_patterns = [rule_pattern]

        for _ in range(3):
            rule_pattern = rule_pattern[-1] + rule_pattern[:-1]
            alt_patterns.append(rule_pattern)

        for rule in self.rules[self.automata[y][x]]:
            rule_i = int(rule[-1])
            if rule[:-1] in alt_patterns:
                expand = None if rule_i == self.automata[y][x] else expand_map
                return rule_i, expand

        return self.automata[y][x], None

    def expand_automata(self, directions: list):
        """
        Expands the automata in the given directions.
        :param directions: list The direction (0, 1, 2, 3) in which to expand.
        :return: None
        """
        for direction in directions:
            if direction & 1 == 0:
                padding = [[0] * len(self.automata[0])]
                if direction == 0:
                    padding += self.automata
                    self.automata = padding
                else:
                    self.automata += padding
            else:
                insert_at = len(self.automata[0]) if direction == 1 else 0
                for i in range(len(self.automata)):
                    self.automata[i].insert(insert_at, 0)

    def step(self):
        """
        Sets each cell into its next step.
        :return: None
        """
        future = [row.copy() for row in self.automata]
        expand_towards = set()
        for y in range(len(self.automata)):
            for x in range(len(self.automata[y])):
                future[y][x], expand = self.get_future_cell(y, x)
                if expand is not None:
                    expand_towards |= set(expand)
        self.automata = future

        if len(expand_towards) > 0:
            self.expand_automata(expand_towards)
