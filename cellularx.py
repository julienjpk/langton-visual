#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#
# Tables coming from https://github.com/GollyGang/ruletablerepository.

from automata import *

from tkinter import *
import sys
import os


class CellularFrame(Frame):
    COLOURS = ("#000000", "#0000FF", "#CC0000", "#009900", "#FFCC00", "#CC00CC",
               "#FFFFFF", "#0099CC")

    def __init__(self, master: Tk, automata: CellularAutomata, interval=100,
                 cell_size=10, colours=COLOURS):
        """
        Initialises the GUI.
        :param master: The Tk master.
        :param automata: The automata from automata.py.
        :param interval: The interval between cellular generations.
        :param cell_size: The size of a cell in pixels.
        :param colours: The colour set (COLOURS).
        """
        if len(colours) != 8:
            raise AttributeError("Invalid colour set (8 colours expected).")

        Frame.__init__(self, master)
        self.automata = automata
        self.interval = interval
        self.cell_size = cell_size
        self.colours = colours
        self.canvas = Canvas(self.master, width=300, height=300)
        self.canvas.pack(fill=BOTH, expand=YES)
        self.canvas.bind("<Configure>", self.on_resize)
        self.master.bind("<space>", self.on_spacebar)

        self.print_error = False
        self.paused = False
        self.scheduled = False
        self.master.update()
        self.paint_canvas()
        self.schedule()

    def get_dimensions(self):
        """
        Returns the dimensions of both the automata, and the canvas.
        :return: The dimensions dictionary.
        """
        grid = self.automata.grid()
        return {'canvas': (self.canvas.winfo_width(),
                           self.canvas.winfo_height()),
                'grid': (self.cell_size * len(grid[0]),
                         self.cell_size * len(grid))}

    def enough_room(self):
        """
        Tells whether there's enough room in the canvas for the automata.
        :return: True of False.
        """
        dim = self.get_dimensions()
        return dim["canvas"][0] >= dim["grid"][0] and \
            dim["canvas"][1] >= dim["grid"][1]

    def on_resize(self, e):
        """
        Handles window resizing based on the drawing state.
        :param e: The Tk event.
        """
        if int(e.type) == 22 and self.enough_room():
            if self.print_error:
                self.print_error = False
                self.schedule()
            elif self.paused:
                self.paint_canvas()

    def on_spacebar(self, e):
        """
        Pauses the drawing.
        """
        self.paused = not self.paused
        if not self.paused:
            self.schedule()

    def schedule(self):
        """
        Starts the drawing process (soon).
        """
        if not self.scheduled:
            self.master.after(self.interval, self.bring_to_life)
            self.scheduled = True

    def bring_to_life(self):
        """
        Brings the automata to its next step, draws, and reschedules.
        """
        self.scheduled = False
        if self.paused:
            return

        self.automata.step()
        if self.paint_canvas():
            self.schedule()
        else:
            self.print_error = True

    def paint_canvas(self):
        """
        Paints the automata onto the canvas.
        :return False if there's not enough room.
        """
        grid = self.automata.grid()
        dim = self.get_dimensions()
        o = (dim['canvas'][0] / 2 - dim['grid'][0] / 2,
             dim['canvas'][1] / 2 - dim['grid'][1] / 2)

        self.canvas.delete("all")
        if not self.enough_room():
            return False

        self.canvas.create_rectangle(0, 0, dim['canvas'][0], dim['canvas'][1],
                                     fill=self.colours[0])

        for y in range(len(grid)):
            for x in range(len(grid[y])):
                p = (o[0] + x * self.cell_size, o[1] + y * self.cell_size)
                p += (p[0] + self.cell_size, p[1] + self.cell_size)
                self.canvas.create_rectangle(p[0], p[1], p[2], p[3],
                                             fill=self.colours[grid[y][x]],
                                             outline=self.colours[0])

        return True


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Usage: %s [rules] [initial grid]." % __file__, file=sys.stderr)
        exit(1)

    try:
        automata = CellularAutomata(sys.argv[1], sys.argv[2])
    except ValueError as e:
        print("An error occured while parsing the rules. %s" % e)
        sys.exit(1)
    except AttributeError as e:
        print("An error occured while parsing the initial grid. %s" % e)
        sys.exit(1)

    master = Tk()
    master.wm_title("Cellular automata: %s" % os.path.basename(sys.argv[1]))
    frame = CellularFrame(master, automata)
    frame.mainloop()
