# Langton loops automata #

[![License](https://img.shields.io/badge/license-GPL-blue.svg)](https://raw.githubusercontent.com/julienjpk/turing-interpreter/master/LICENSE)

Rather simple GUI displaying the evolution of a cellular automata expanding
[Langton loops](https://en.wikipedia.org/wiki/Langton%27s_loops). Press the
space bar to pause/resume it. That's pretty much all the features.

The table for the Langton loops rules comes from here : 
https://github.com/GollyGang/ruletablerepository ; **thanks!**

Run as:

    ./cellularx.py langton_loops.table langton_initial.dat
    
And that's it!
